package com.example.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

// api 서버인증
@EnableResourceServer
// oauth2 인증서버 활성화
@EnableAuthorizationServer
@SpringBootApplication
public class AuthApplication extends ResourceServerConfigurerAdapter{

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
		// header 충돌 방지
		.headers().frameOptions().disable()
		.and()
		.authorizeRequests().antMatchers("/", "/api/**").authenticated();
	}
}

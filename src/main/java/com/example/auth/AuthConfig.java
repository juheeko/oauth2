package com.example.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.client.InMemoryClientDetailsService;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.sql.DataSource;

/**
 * 클라이언트, 토큰관리 - spring security oauth
 * Created by juheeko on 02/07/2018.
 */
@Configuration
@EnableAuthorizationServer
public class AuthConfig extends AuthorizationServerConfigurerAdapter{

    // client id
    @Value("${spring.security.oauth2.client.registration.my-client.client-id}")
    private String clientId;

    // client secret
    @Value("${spring.security.oauth2.client.registration.my-client.client-secret}")
    private String clientSecret;

//    @Value("${spring.redis.host}")
//    private String redisHost;
//
//    @Value("${spring.redis.port}")
//    private Integer redisPort;

    @Autowired
    private DataSource dataSource;

    // client
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {


        // inMemory / jdbc중
        clients.jdbc(dataSource)
                // client id
                .withClient(clientId)
                // client secret
                .secret(clientSecret)
                .scopes("read:current_user")
                // access_token을 얻기위한 4가지 방법중 하나 client_id, client_secret
                .authorizedGrantTypes("client_credentials");
    }

    // client_id, client_secret 저장하는 클라이언트 저장소에 대한 CRUD => ClientDetailsService 인터페이스로 구현
    @Bean
    public ClientDetailsService clientDetailsService() {
        return new InMemoryClientDetailsService();
    }

    // token저장
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception{

        endpoints.tokenStore(tokenStore());
    }

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }
//    @Bean
//    public TokenStore tokenStore() {
//        return new RedisTokenStore(jedisConnectionFactory());
//    }

//    @Bean
//    public JedisConnectionFactory jedisConnectionFactory() {
//
//        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
//        config.setHostName(redisHost);
//        config.setPort(redisPort);
////        config.setPassword("");
//        config.setDatabase(1);
//
//        JedisClientConfiguration clientConfig = new JedisClientConfiguration();
//        return new JedisConnectionFactory(config);
//    }
}

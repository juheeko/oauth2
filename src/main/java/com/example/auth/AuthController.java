package com.example.auth;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by juheeko on 03/07/2018.
 */

@RestController
public class AuthController {

    @GetMapping(value ="/")
    public String index() throws Exception{
        return "Oauth2";
    }
}
